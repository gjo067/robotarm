#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

#include "arm/armcontroller.h"


class Window;
class Joint;

class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;
  void    setUpSignals(Window& window);

signals:
  void sendPorts(QVariant)/*{qDebug("signal in C++");}*/;

public slots:
  void callDefferedGL();  

  void testButPress();
  void testIKButPress();
private:
  ArmController m_armController;
  std::shared_ptr<Joint> test_obj;
};


#endif // SCENARIO_H
