#ifndef MESH_H
#define MESH_H

#include <scene/gmsceneobject.h>

#include "texture.h"

class MeshVisualizer;

class Mesh : public GMlib::SceneObject
{
        GM_SCENEOBJECT(Mesh)
public:
    Mesh();
    Mesh(const Mesh& copy);
    ~Mesh() override;

    void createMesh(std::string name, std::vector<float>& vs, std::vector<unsigned int>& is);

    // Setters
    void setTexture(Texture* tex);
    void setSpeed(double speed);
    void setAngle(GMlib::Angle angle, bool animate = true);
    void setAngleDelta(GMlib::Angle angleDelta, bool animate = true);
    void setAngleLimits(GMlib::Angle min, GMlib::Angle max) { m_minAngle = min; m_maxAngle = max; }
    void setRotAxel(GMlib::Vector<float, 3> rotAxel);
    void setPos(GMlib::Vector<float, 3> pos, bool animate = true);

    // Getters
    std::string getName() { return m_name; }
    bool isExecuting() { return m_isExecuting; }
    GMlib::Angle getAngle() { return m_currAngle; }
	
    GMlib::Vector<float, 3> getAxel() {return m_rotAxel; }
    GMlib::Vector<float, 3> getCurrentPos() { return getGlobalPos(); }

    // Returns the smallest difference between two angles.
    GMlib::Angle static getAngleDiff(GMlib::Angle current_angle, GMlib::Angle desired_angle);

protected:
    void localSimulate(double dt) override;

private:
    MeshVisualizer* m_default_visualizer;
    std::string m_name;

    bool m_isExecuting;
    double m_speed;

    GMlib::Angle m_currAngle;
    GMlib::Angle m_targetAngle;
    GMlib::Angle m_minAngle;
    GMlib::Angle m_maxAngle;
    GMlib::Vector<float, 3> m_rotAxel;

    GMlib::Vector<float, 3> m_currPos;
    GMlib::Vector<float, 3> m_targetPos;

    void clampTargetAngle();
};

#endif // MESH_H
