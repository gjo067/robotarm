#ifndef TEXTUREDMESHSHADER_H
#define TEXTUREDMESHSHADER_H

#include <opengl/gmprogram.h>

class TexturedMeshShader
{
public:
    TexturedMeshShader();

private:
    GMlib::GL::Program _prog;
};

#endif // TEXTUREDMESHSHADER_H
