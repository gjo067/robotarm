#define STB_IMAGE_IMPLEMENTATION

#include "texture.h"

#include "stb_image.h"

#include <iostream>

Texture::Texture()
    : m_textureID(0), m_width(0), m_height(0), m_bitDepth(0)
{
//    std::cout << "Created new Texture" << std::endl;
}

Texture::~Texture()
{
    glDeleteTextures(1, &m_textureID);
    m_textureID = 0;
    m_width = 0;
    m_height = 0;
    m_bitDepth = 0;
    m_bitDepth = 0;
//    std::cout << "Deleted Texture" << std::endl;
}

bool Texture::loadTexture(std::string file)
{
    unsigned char* texData = stbi_load(file.c_str(), &m_width, &m_height, &m_bitDepth, 0);
    if(!texData)
    {
        std::cerr << "Failed to load texture: " << file << std::endl;
        return false;
    }

    glGenTextures(1, &m_textureID);
    glBindTexture(GL_TEXTURE_2D, m_textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texData);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(texData);

    return true;
}

void Texture::useTexture(GLenum location)
{
    glActiveTexture(location);
    glBindTexture(GL_TEXTURE_2D, m_textureID);
}
