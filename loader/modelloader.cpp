#define TINYOBJLOADER_IMPLEMENTATION

#include "modelloader.h"

#include "tiny_obj_loader.h"

#include <iostream>
#include <chrono>

// Faster than Assimp, but does not delete duplicate vertices!
Model ModelLoader::LoadModelObj(const std::string& baseDir, const std::string& fileName)
{
    std::cout << "Loading model " << baseDir << fileName << " using tiny_obj_loader" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    // https://github.com/syoyo/tinyobjloader
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string warn;
    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, (baseDir + fileName).c_str(), baseDir.c_str());

    if(!warn.empty()) {
        std::cout << "TinyObjLoader Warning: " << warn << std::endl;
    }

    if(!err.empty()) {
        std::cerr << "TinyObjLoader Error: " << err << std::endl;
    }

    if(!ret) {
        exit(1);
    }

    std::unordered_map<std::string, Mesh*> meshMap;
    std::vector<Texture*> textures;

    // Load materials
    for(size_t m = 0; m < materials.size(); m++) {
        textures.push_back(new Texture());
        std::string filename;
        if(materials[m].diffuse_texname == "") {
            filename = "plain.png";
        }
        else {
            size_t idx = std::string(materials[m].diffuse_texname).rfind("\\");
            filename = std::string(materials[m].diffuse_texname).substr(idx + 1);
        }
        std::string texPath = std::string("../../robotarm/textures/") + filename;
        textures[m]->loadTexture(texPath);
    }

    int totalsize = 0;

    // Load meshes
    // TODO: Remove duplicate vertices.
    for(size_t s = 0; s < shapes.size(); s++) {
        Mesh* newMesh = new Mesh();
        std::vector<float> vertices;
        std::vector<unsigned int> indices;

        for(size_t i = 0; i < shapes[s].mesh.indices.size(); i++) {
            tinyobj::index_t idx = shapes[s].mesh.indices[i];
            vertices.push_back(attrib.vertices[3*idx.vertex_index + 0]);    // vx
            vertices.push_back(attrib.vertices[3*idx.vertex_index + 1]);    // vy
            vertices.push_back(attrib.vertices[3*idx.vertex_index + 2]);    // vz
            vertices.push_back(attrib.texcoords[2*idx.texcoord_index + 0]);  // tu
            vertices.push_back(1.0f - attrib.texcoords[2*idx.texcoord_index + 1]);  // tv
            vertices.push_back(attrib.normals[3*idx.normal_index + 0]);    // nx
            vertices.push_back(attrib.normals[3*idx.normal_index + 1]);    // ny
            vertices.push_back(attrib.normals[3*idx.normal_index + 2]);    // nz
            indices.push_back(i);
        }

        totalsize += vertices.size();

        newMesh->setTexture(textures[shapes[s].mesh.material_ids[0]]);
        newMesh->createMesh(shapes[s].name, vertices, indices);
        meshMap.insert({newMesh->getName(), newMesh});
    }

    auto end = std::chrono::high_resolution_clock::now();
    double duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    std::cout << "Finished loading model, elapsed time: " << duration / 1000.0 << "s" << std::endl;
    std::cout << "Total polygons: " << totalsize / 24 << std::endl;

    return {meshMap, textures};
}
