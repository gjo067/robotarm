import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.4

import "qrc:/qml/components"

import SceneGraphRendering 1.0

Item {
  id: root

  property var posInputWindow;
  property var chartWindow;

  signal toggleHidBindView
  signal arm1SliderMoved(int angle);
  signal arm2SliderMoved(int angle);
  signal arm3SliderMoved(int angle);
  signal baseSliderMoved(int angle);
  signal handSpaceSliderMoved(int space);
  signal speedSpinChanged(int speed);
  signal moveRealArmChanged(int sheckstatus);
  signal connectToArm(string port, int baud, int data_bits, int stop_bits, int parity, int flow);

  signal shouldGripperTrack(bool shouldTrack);
  signal shouldEefParallel();
  signal showJoints(bool shouldShow);
  signal showPencil(bool shouldShow);

  signal testButPress();
  signal testIKButPress();
  signal randomStepArm();
  signal drawSquareArm();
  signal drawParametricArm();
  signal drawSpiralArm();
  signal drawText(string text);

  signal executeActions(var coords);

  onToggleHidBindView: hid_bind_view.toggle()

  function onCoordinatesReceived(coords) {
      if(coords.length > 0) {
          executeActions(coords);
      }
  }

  function showPorts(in_ports)
  {
      console.log("slot in qml "+in_ports[0]);
      for (var i = 0; i<in_ports.length;i++)
      {
          portCBitems.append({"key":in_ports[i],"value":in_ports[i]})
      }
  }

  function baseSliderChanged(in_value)
  {
      base_angle_slider.value = in_value
  }
  function arm1SliderChanged(in_value)
  {
      arm1_angle_slider.value = in_value
  }
  function arm2SliderChanged(in_value)
  {
      arm2_angle_slider.value = in_value
  }
  function arm3SliderChanged(in_value)
  {
      arm3_angle_slider.value = in_value
  }
  function handSliderChanged(in_value)
  {
      console.log("slot in qml ");
  }

  function sendSlidersToMotors()
  {
      speedSpinChanged(speed_spinbox.value)
      arm1SliderMoved(arm1_angle_slider.value);
      arm2SliderMoved(arm2_angle_slider.value);
      arm3SliderMoved(arm3_angle_slider.value);
      baseSliderMoved(base_angle_slider.value);
      handSpaceSliderMoved(hand_space_slider.value);
  }

  function eefPosChanged(px, py, pz) {
      if(chartWindow) {
          chartWindow.addPoint(px, py, pz);
      }
  }

  function clearPlots() {
      if(chartWindow) {
          chartWindow.clearPlots();
      }
  }

  GridLayout {
      id: gridLay
      columns: 2
      rows: 1
      anchors.fill: parent
      columnSpacing: 0.0
      rowSpacing: 0.0

      Rectangle {
        id: renderField
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.columnSpan: 1
        Layout.rowSpan: 1
        Layout.column: 1
        Layout.row: 1


//        width:500
        color: "lightgray"

        Renderer {
            id: renderer

            anchors.fill: parent

            rcpair_name: rc_pair_cb.currentText

            ComboBox {
              id: rc_pair_cb
              anchors.top: parent.top
              anchors.left: parent.left
              anchors.margins: 5

              width: 128

              opacity: 0.7

              model: rc_name_model
              textRole: "display"
              currentIndex: 0
            }

            Button {
              text: "?"
              anchors.top: parent.top
              anchors.right: parent.right
              anchors.margins: 5

              width: height
              opacity: 0.7

              onClicked: hid_bind_view.toggle()
            }

            HidBindingView {
              id: hid_bind_view
              anchors.fill: parent
              anchors.margins: 50
              visible:false

              states: [
                State{
                  name: "show"
                  PropertyChanges {
                    target: hid_bind_view
                    visible: true
                  }
                }
              ]

              function toggle() {

                if(state === "") state = "show"
                else state = ""

              }
            }
        }
      }

      Rectangle {
        width:300
        color:  "lightgray"
        id: rightColumn
        Layout.fillHeight: true
        Layout.fillWidth: true

        Layout.columnSpan: 1
        Layout.rowSpan: 1
        Layout.column: 2
        Layout.row: 1
        Layout.maximumWidth: 250


      Rectangle {
        width:250
        height:200
        color:  "lightgray"

        id: connectionSetup
        Layout.fillHeight: true
        Layout.fillWidth: true
//        Layout.columnSpan: 1
//        Layout.rowSpan: 1
        Layout.maximumWidth: 250

        anchors.top:parent.top

        Text {
            id: connectionSetupTxt
            text: qsTr("Connection Setup")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        ComboBox {
            id: portCB
            width:110
            anchors.topMargin: 5
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.top:connectionSetupTxt.bottom

            currentIndex: 2
            displayText: "Port"
            textRole: "key"
            model: ListModel
            {
                id:portCBitems
            }
        }

        ComboBox {
            id: baudCB
            width:110
            anchors.topMargin: 5
            anchors.rightMargin: 10
            anchors.right: parent.right
            anchors.top:connectionSetupTxt.bottom

            displayText: "Baud"
//            onActivated: displayText=currentText

            currentIndex: 3
            textRole: "key"
            model: ListModel {
                ListElement {key:"1200";value:1200}
                ListElement {key:"2400";value:2400}
                ListElement {key:"4800";value:4800}
                ListElement {key:"9600";value:9600}
                ListElement {key:"19200";value:19200}
                ListElement {key:"38400";value:38400}
                ListElement {key:"57600";value:57600}
                ListElement {key:"115200";value:115200}
            }
        }

        ComboBox {
            id: dataCB
            width:110
            anchors.topMargin: 5
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.top:portCB.bottom

            currentIndex: 3
            displayText: "Data bit"
            model: [5, 6, 7, 8]
//            onActivated: displayText=currentText
        }

        ComboBox {
            id: stopCB
            width:110
            anchors.topMargin: 5
            anchors.rightMargin: 10
            anchors.right: parent.right
            anchors.top:portCB.bottom

            displayText: "Stop bit"

            currentIndex: 0
            textRole: "key"
            model: ListModel {
                ListElement {key:"1";value:1}
                ListElement {key:"1.5";value:3}
                ListElement {key:"2";value:2}
            }

        }

        ComboBox {
            id: parityCB
            width:110
            anchors.topMargin: 5
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.top:dataCB.bottom

            displayText: "Parity"
            currentIndex: 0
            textRole: "key"
            model: ListModel {
                ListElement {key:"No";value:0}
                ListElement {key:"Even";value:2}
                ListElement {key:"Odd";value:3}
                ListElement {key:"Space";value:4}
                ListElement {key:"Mark";value:5}
            }
        }

        ComboBox {
            id: flowCB
            width:110
            anchors.topMargin: 5
            anchors.rightMargin: 10
            anchors.right: parent.right
            anchors.top:dataCB.bottom

            displayText: "Flow cont."
            currentIndex: 0
            textRole: "key"
            model: ListModel {
                ListElement {key:"No";value:0}
                ListElement {key:"Hardware";value:1}
                ListElement {key:"Software";value:2}
            }
        }

        Switch {
            id: connect_btn
            text: "Connect"
            font.pointSize: 6

            width:115

            //scale: 0.8

            anchors.leftMargin: 5
            anchors.left: parent.left
            anchors.top:parityCB.bottom

            onCheckedChanged: connect_btn.connectToArmPress()

            function connectToArmPress()
            {
                //connect_btn.pressed=!connect_btn.pressed
                connect_btn.state=!connect_btn.state
                portCB.enabled=!portCB.enabled
                baudCB.enabled=!baudCB.enabled
                dataCB.enabled=!dataCB.enabled
                stopCB.enabled=!stopCB.enabled
                parityCB.enabled=!parityCB.enabled
                flowCB.enabled=!flowCB.enabled
                move_real_cb.enabled=!move_real_cb.enabled

                if (connect_btn.checked)
                {
                    //console.log(dataCB.model[dataCB.currentIndex])
                    connectToArm(portCB.model.get(portCB.currentIndex).value,
                                 baudCB.model.get(baudCB.currentIndex).value,
                                 dataCB.model[dataCB.currentIndex],
                                 stopCB.model.get(stopCB.currentIndex).value,
                                 parityCB.model.get(parityCB.currentIndex).value,
                                 flowCB.model.get(flowCB.currentIndex).value)
                    root.sendSlidersToMotors()
                }
                else
                {
//                  have to call disconnect, which closes connection to serial port
                    move_real_cb.checkState=Qt.Unchecked
                }
            }
        }

        CheckBox {
            id: move_real_cb
            text: "Move real"

            checkState: Qt.Unchecked

            enabled: false

            anchors.left: connect_btn.right
            anchors.leftMargin: 5
            anchors.top: parityCB.bottom

            onCheckStateChanged: move_real_cb.moveRealCheck()

            function moveRealCheck()
            {
                moveRealArmChanged(move_real_cb.checkState);
                if (this.checkState === Qt.Checked)
                {
                    root.sendSlidersToMotors()
                }
            }
        }

      }

            Rectangle {
              width:parent.width
              height:340
              color: "lightgray"

              id: directControls
              Layout.fillHeight: true
              Layout.fillWidth: true
//              Layout.columnSpan: 1
//              Layout.rowSpan: 1
              Layout.maximumWidth: 250

              anchors.top: connectionSetup.bottom

              Text {
                  id: directControlsTxt
                  text: qsTr("Arm Control")
                  anchors.horizontalCenter: parent.horizontalCenter
              }

              Text {
                  id: joint0Txt
                  text: qsTr("Joint 0")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: base_angle_slider.verticalCenter
              }

              Slider {
                  id: base_angle_slider

                  anchors.right: parent.right
                  anchors.rightMargin: 10
                  anchors.top: directControlsTxt.bottom
                  width: 180

                  from: 0
                  value: 90
                  //to: 180
                  to: 165

                  onMoved: baseSliderMoved(base_angle_slider.value)
                  //onMoved: baseSliderMoved(base_angle_slider.value)
              }

              Text {
                  id: joint1Txt
                  text: qsTr("Joint 1")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: arm1_angle_slider.verticalCenter
              }

              Slider {
                  id: arm1_angle_slider

                  anchors.right: parent.right
                  anchors.rightMargin: 10
                  anchors.top: base_angle_slider.bottom
                  width: 180

                  from: 0
                  value: 90
                  to: 180

                  onMoved: arm1SliderMoved(arm1_angle_slider.value)
                  //onMoved: arm1SliderMoved(arm1_angle_slider.value)
              }

              Text {
                  id: joint2Txt
                  text: qsTr("Joint 2")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: arm2_angle_slider.verticalCenter
              }

              Slider {
                  id: arm2_angle_slider

                  anchors.right: parent.right
                  anchors.rightMargin: 10
                  anchors.top: arm1_angle_slider.bottom
                  width: 180

                  from: 0
                  value: 90
                  to: 165

                  onMoved: arm2SliderMoved(arm2_angle_slider.value)
                  //onMoved: arm2SliderMoved(arm2_angle_slider.value)
              }

              Text {
                  id: joint3Txt
                  text: qsTr("Joint 3")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: arm3_angle_slider.verticalCenter
              }

              Slider {
                  id: arm3_angle_slider

                  anchors.right: parent.right
                  anchors.rightMargin: 10
                  anchors.top: arm2_angle_slider.bottom
                  width: 180

                  from: 0
                  value: 90
                  to: 180

                  onMoved: arm3SliderMoved(arm3_angle_slider.value)
                  //onMoved: arm3SliderMoved(arm3_angle_slider.value)
              }

              Text {
                  id: joint4Txt
                  text: qsTr("Gripper")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: hand_space_slider.verticalCenter
              }

              Slider {
                  id: hand_space_slider

                  anchors.right: parent.right
                  anchors.rightMargin: 10
                  anchors.top: arm3_angle_slider.bottom
                  width: 180

                  from: 0
                  value: 15
                  to: 30

                  onMoved: handSpaceSliderMoved(hand_space_slider.value)
                  //onMoved: handSpaceSliderMoved(hand_space_slider.value)
              }

              Text {
                  id: speedTxt
                  text: qsTr("Speed")
                  anchors.left: parent.left
                  anchors.leftMargin: 10
                  anchors.verticalCenter: speed_spinbox.verticalCenter
              }

              SpinBox {
                id: speed_spinbox
                scale: 0.8

                anchors.left: speedTxt.right
                anchors.top: hand_space_slider.bottom
                width: 120

                from: 100
                value: 300
                to: 2000
                stepSize: 100

                onValueChanged: speed_spinbox.speedChange()
                function speedChange()
                {
                    speedSpinChanged(speed_spinbox.value)
                    root.sendSlidersToMotors()
                }
              }


              Button {
                  id: center_arm_btn
                  text: "center"

                  scale: 0.8

                  anchors.left: speed_spinbox.right
                  anchors.leftMargin: -15
                  anchors.verticalCenter: speed_spinbox.verticalCenter

                  onClicked: center_arm_btn.centerSliders()

                  function centerSliders()
                  {
//                      base_angle_slider.value = (base_angle_slider.to-base_angle_slider.from)/2
//                      arm1_angle_slider.value = (arm1_angle_slider.to-arm1_angle_slider.from)/2
//                      arm2_angle_slider.value = (arm2_angle_slider.to-arm2_angle_slider.from)/2
//                      arm3_angle_slider.value = (arm3_angle_slider.to-arm3_angle_slider.from)/2
                      hand_space_slider.value = (hand_space_slider.to-hand_space_slider.from)/2
                      base_angle_slider.value = 90
                      arm1_angle_slider.value = 90
                      arm2_angle_slider.value = 90
                      arm3_angle_slider.value = 90
//                      to be sure that signals called
                      baseSliderMoved(base_angle_slider.value)
                      arm1SliderMoved(arm1_angle_slider.value)
                      arm2SliderMoved(arm2_angle_slider.value)
                      arm3SliderMoved(arm3_angle_slider.value)
                      handSpaceSliderMoved(hand_space_slider.value)

                  }

              }

              Text {
                  id: path_track_desc
                  text: qsTr("Track")

                  anchors.left: path_track_gripper.right
//                  anchors.leftMargin: 10
                  anchors.top: speed_spinbox.bottom
                  anchors.topMargin: 15
              }

              CheckBox {
                  id: path_track_gripper
                  //text: "Gripper"

                  checkState: Qt.Unchecked

                  anchors.left: parent.left
//                  anchors.leftMargin: 5
                  anchors.top: speed_spinbox.bottom

                  onCheckStateChanged: shouldGripperTrack(path_track_gripper.checkState)
              }


              Text {
                  id: parallel_text
                  text: qsTr("Eef parallel to XZ")

                  anchors.right: center_arm_btn.right
                  anchors.rightMargin: 10
                  anchors.top: speed_spinbox.bottom
                  anchors.topMargin: 15
              }

              CheckBox {
                  id: parallel_eef

                  checkState: Qt.Unchecked

                  anchors.right: parallel_text.left
//                  anchors.leftMargin: 5
                  anchors.top: speed_spinbox.bottom

                  onCheckStateChanged: shouldEefParallel()
              }

              Text {
                  id: balls_text
                  text: "Joints"

                  anchors.left: show_balls_checkbox.right
//                  anchors.leftMargin: 10
                  anchors.top: path_track_desc.bottom
                  anchors.topMargin: 18
              }

              CheckBox {
                  id: show_balls_checkbox
                  checkState: Qt.Checked
                  anchors.left: parent.left
//                  anchors.leftMargin: -10
                  anchors.top: path_track_desc.bottom
                  anchors.topMargin: 5

                  onCheckStateChanged: showJoints(show_balls_checkbox.checkState);
              }

              Text {
                  id: pencil_text
                  text: "Pencil"

                  anchors.left: pencil_checkbox.right
//                  anchors.leftMargin: 10
                  anchors.top: path_track_desc.bottom
                  anchors.topMargin: 18
              }

              CheckBox {
                  id: pencil_checkbox
                  checkState: Qt.Unchecked
                  anchors.left: parallel_eef.left
//                  anchors.leftMargin: -10
                  anchors.top: path_track_desc.bottom
                  anchors.topMargin: 5

                  onCheckStateChanged: showPencil(pencil_checkbox.checkState);
              }


            }

            Rectangle {
              width:parent.width
              color: "lightgray"

              id: extraFunctions
              Layout.fillHeight: true
              Layout.fillWidth: true
              Layout.maximumWidth: 250

              anchors.top: directControls.bottom

              Text {
                  id: extraFunctionsTxt
                  text: qsTr("Additional")
                  anchors.horizontalCenter: parent.horizontalCenter
              }

              Button {
                  id: test_but
                  text: "Set goal"

                  scale: 0.8

                  anchors.top: extraFunctionsTxt.bottom
                  anchors.left: parent.left
//                  anchors.leftMargin: 10

                  onClicked: testButPress()

              }

              Button {
                  id: test_ik_but
                  text: "Go to goal"

                  scale: 0.8

                  anchors.top: extraFunctionsTxt.bottom
                  anchors.left: test_but.right
                  anchors.leftMargin: 10

                  onClicked: testIKButPress()

              }

              Button {
                  id: random_step_but
                  text: "Random step"

                  scale: 0.8
                  anchors.left: parent.left
                  anchors.top: test_but.bottom

                  onClicked: {
                      randomStepArm()
                  }
              }

              Button {
                  id: coord_input_btn
                  text: "Input Pos"

                  scale: 0.8
                  anchors.left: random_step_but.right
                  anchors.leftMargin: 10
                  anchors.top: test_but.bottom

                  onClicked: {
                      if(!posInputWindow) {
                          var component = Qt.createComponent("components/CoordInputWindow.qml");
                          posInputWindow = component.createObject(root, {rootWindow: root, callback: onCoordinatesReceived});
                          posInputWindow.show();
                      }
                      else {
                          posInputWindow.showNormal();
                      }
                  }
              }

              Button {
                  id: draw_square_but
                  text: "Square"

                  scale: 0.8
                  anchors.left: parent.left
//                  anchors.leftMargin: 10
                  anchors.top: coord_input_btn.bottom

                  onClicked: {
                      drawSquareArm()
                  }
              }

              Button {
                  id: draw_parametric_but
                  text: "Parametric"

                  scale: 0.8
                  anchors.left: draw_square_but.right
                  anchors.leftMargin: 10
                  anchors.top: coord_input_btn.bottom

                  onClicked: {
                      drawParametricArm()
                  }
              }

              Button {
                  id: chart_btn
                  text: "Plots"

//                  width: 50
                  scale: 0.8
                  anchors.left: parent.left
//                  anchors.leftMargin: 10
                  anchors.top: draw_square_but.bottom

                  onClicked: {
                      if(chartWindow) {
                          chartWindow.showNormal();
                      }
                      else {
                          var component = Qt.createComponent("components/ChartPosWindow.qml");

                          if(component.status === Component.Ready) {
                              chartWindow = component.createObject(root, {rootWindow: root});
                              chartWindow.show();
                          }
                      }
                  }
              }

              Button {
                  id: draw_spiral_btn
                  text: "Spiral"

                  scale: 0.8
                  anchors.left: chart_btn.right
                  anchors.leftMargin: 10
                  anchors.top: draw_square_but.bottom

                  onClicked: {
                      drawSpiralArm();
                  }
              }

              Rectangle {
                  id: input_text_rect

                  anchors.left: chart_btn.left
                  anchors.leftMargin: 10
                  anchors.right: chart_btn.right
                  anchors.rightMargin: 10
                  anchors.top: draw_spiral_btn.bottom
                  anchors.topMargin: 5

                  border.width: 1

                  height: 30

                  TextInput {
                      id: input_text

                      font.pointSize: 14

//                      anchors.fill: parent
                      anchors.centerIn: parent
                      height: parent.height - 3
                      width: parent.width - 3
                  }
              }

              Button {
                  id: write_text_btn
                  text: "Write"

                  scale: 0.8
                  anchors.left: draw_spiral_btn.left
                  anchors.leftMargin: 0
                  anchors.top: draw_spiral_btn.bottom

                  onClicked: {
                      drawText(input_text.text);
                  }
              }
            }
      }

    }
}

