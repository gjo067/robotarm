import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtCharts 2.13

Window {
    id: window

    title: "EEF Position Plots"

    width: 800
    height: 800

    property var rootWindow;
    property var addPoint: addCoord;

    property var maxRadius: 500;

    function addCoord(px, py, pz) {
        line_series_xy.append(px, py);
        line_series_xz.append(px, pz);
        line_series_yz.append(pz, py);
    }

    function clearPlots() {
        line_series_xy.removePoints(0, line_series_xy.count);
        line_series_xz.removePoints(0, line_series_xz.count);
        line_series_yz.removePoints(0, line_series_yz.count);
    }

    onClosing: {
        rootWindow.chartWindow = null;
    }

    Rectangle {
        anchors.fill: parent

        color: "LightGrey"

        Button {
            text: "Clear"
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10

            onClicked: {
                clearPlots();
            }
        }

        ChartView {
            id: chart_view_xz
            anchors.left: parent.left
            anchors.leftMargin: 200
            anchors.top: parent.top
            antialiasing: true

            width: 400
            height: 400

            MouseArea {
                anchors.fill: parent
                onClicked: { chart_view_xz.focus = true; }
            }
            Keys.onPressed: {
                if(event.key === Qt.Key_Plus) {
                    chart_view_xz.zoomIn();
                    axisX.tickInterval /= 2;
                    axisY.tickInterval /= 2;
                }
                else if(event.key === Qt.Key_Minus) {
                    chart_view_xz.zoomOut();
                    axisX.tickInterval *= 2;
                    axisY.tickInterval *= 2;
                }
                else if(event.key === Qt.Key_Left) { chart_view_xz.scrollLeft(15); }
                else if(event.key === Qt.Key_Right) { chart_view_xz.scrollRight(15); }
                else if(event.key === Qt.Key_Up) { chart_view_xz.scrollUp(15); }
                else if(event.key === Qt.Key_Down) { chart_view_xz.scrollDown(15); }
            }

            ValueAxis {
                id: axisX
                titleText: "X"
                min: -maxRadius
                max: maxRadius
                tickInterval: 250
                tickType: ValueAxis.TicksDynamic
                tickAnchor: -500
                minorTickCount: 4
            }

            ValueAxis {
                id: axisY
                titleText: "Z"
                min: -maxRadius
                max: maxRadius
                tickInterval: 250
                tickType: ValueAxis.TicksDynamic
                tickAnchor: -500
                minorTickCount: 4
            }

            LineSeries {
                id: line_series_xz
                axisX: axisX
                axisY: axisY
                name: "XZ Plot"

                color: "Red"
            }
        }

        ChartView {
            id: chart_view_xy
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            antialiasing: true

            width: 400
            height: 400

            MouseArea {
                anchors.fill: parent
                onClicked: { chart_view_xy.focus = true; }
            }
            Keys.onPressed: {
                if(event.key === Qt.Key_Plus) {
                    chart_view_xy.zoomIn();
                    axisX2.tickInterval /= 2;
                    axisY2.tickInterval /= 2;
                }
                else if(event.key === Qt.Key_Minus) {
                    chart_view_xy.zoomOut();
                    axisX2.tickInterval *= 2;
                    axisY2.tickInterval *= 2;
                }
                else if(event.key === Qt.Key_Left) { chart_view_xy.scrollLeft(15); }
                else if(event.key === Qt.Key_Right) { chart_view_xy.scrollRight(15); }
                else if(event.key === Qt.Key_Up) { chart_view_xy.scrollUp(15); }
                else if(event.key === Qt.Key_Down) { chart_view_xy.scrollDown(15); }
            }

            ValueAxis {
                id: axisX2
                titleText: "X"
                min: -maxRadius
                max: maxRadius
                tickInterval: 250
                tickType: ValueAxis.TicksDynamic
                tickAnchor: -500
                minorTickCount: 4
            }

            ValueAxis {
                id: axisY2
                titleText: "Y"
                min: 0
                max: maxRadius
                tickInterval: 100
                tickType: ValueAxis.TicksDynamic
                tickAnchor: 0
                minorTickCount: 4
            }

            LineSeries {
                id: line_series_xy
                axisX: axisX2
                axisY: axisY2
                name: "XY Plot"

                color: "Green"
            }
        }

        ChartView {
            id: chart_view_yz
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            antialiasing: true

            width: 400
            height: 400

            MouseArea {
                anchors.fill: parent
                onClicked: { chart_view_yz.focus = true; }
            }
            Keys.onPressed: {
                if(event.key === Qt.Key_Plus) {
                    chart_view_yz.zoomIn();
                    axisX3.tickInterval /= 2;
                    axisY3.tickInterval /= 2;
                }
                else if(event.key === Qt.Key_Minus) {
                    chart_view_yz.zoomOut();
                    axisX3.tickInterval *= 2;
                    axisY3.tickInterval *= 2;
                }
                else if(event.key === Qt.Key_Left) { chart_view_yz.scrollLeft(15); }
                else if(event.key === Qt.Key_Right) { chart_view_yz.scrollRight(15); }
                else if(event.key === Qt.Key_Up) { chart_view_yz.scrollUp(15); }
                else if(event.key === Qt.Key_Down) { chart_view_yz.scrollDown(15); }
            }

            ValueAxis {
                id: axisX3
                titleText: "Z"
                min: -maxRadius
                max: maxRadius
                tickInterval: 250
                tickType: ValueAxis.TicksDynamic
                tickAnchor: -500
                minorTickCount: 4
            }

            ValueAxis {
                id: axisY3
                titleText: "Y"
                min: 0
                max: maxRadius
                tickInterval: 100
                tickType: ValueAxis.TicksDynamic
                tickAnchor: 0
                minorTickCount: 4
            }

            LineSeries {
                id: line_series_yz
                axisX: axisX3
                axisY: axisY3
                name: "ZY Plot"

                color: "Blue"
            }
        }
    }
}
