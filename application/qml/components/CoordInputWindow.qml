import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

Window {
    id: window

    title: "Coordinate Input"

    width: 300
    height: 400

    property var rootWindow;
    property var callback;

    onClosing: {
        rootWindow.posInputWindow = null;
    }

    Rectangle {
        anchors.fill: parent

        color: "LightGrey"

        Text {
            id: coords_list_header
            text: "Coordinates:"

            font.pointSize: 20

            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 10
        }

        Text {
            id: input_x_label
            text: "x: "

            font.pointSize: 16

            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10
        }

        Rectangle {
            id: input_x_rect

            anchors.left: input_x_label.right
            anchors.leftMargin: 0
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10

            border.width: 2
            radius: 5

            width: 45
            height: 30

            TextInput {
                id: input_x

                validator: IntValidator{bottom: -500; top: 500;}

                anchors.centerIn: parent
                width: 40

                KeyNavigation.tab: input_y
            }
        }

        Text {
            id: input_y_label
            text: "y: "

            font.pointSize: 16

            anchors.left: input_x_rect.right
            anchors.leftMargin: 10
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10
        }

        Rectangle {
            id: input_y_rect

            anchors.left: input_y_label.right
            anchors.leftMargin: 0
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10

            border.width: 2
            radius: 5

            width: 45
            height: 30

            TextInput {
                id: input_y

                validator: IntValidator{bottom: 0; top: 500;}

                anchors.centerIn: parent
                width: 40

                KeyNavigation.tab: input_z
            }
        }

        Text {
            id: input_z_label
            text: "z: "

            font.pointSize: 16

            anchors.left: input_y_rect.right
            anchors.leftMargin: 10
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10
        }

        Rectangle {
            id: input_z_rect

            anchors.left: input_z_label.right
            anchors.leftMargin: 0
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10

            border.width: 2
            radius: 5

            width: 45
            height: 30

            TextInput {
                id: input_z

                validator: IntValidator{bottom: -500; top: 500;}

                anchors.centerIn: parent
                width: 40

                KeyNavigation.tab: input_x
            }
        }

        Button {
            id: add_coord_btn
            text: "Add"

            width: 40
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.left: input_z_rect.right
            anchors.leftMargin: 5
            anchors.top: coords_list_header.bottom
            anchors.topMargin: 10

            onClicked: {
                coords_model.append({"px": input_x.text, "py": input_y.text, "pz": input_z.text})
            }

//            Keys.onEnterPressed: {
//                console.log("asd");
//                clicked()
//            }

//            KeyNavigation.tab: input_x
        }

        Button {
            id: delete_coord_btn
            text: "Delete"

            width: 50
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: add_coord_btn.bottom
            anchors.topMargin: 10

            onClicked: {
                if(coords_list.currentIndex !== -1) {
                    coords_model.remove(coords_list.currentIndex);
                }
            }
        }

        Button {
            id: move_up_coord_btn
            text: "Up"

            width: 50
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: delete_coord_btn.bottom
            anchors.topMargin: 10

            onClicked: {
                if(coords_list.currentIndex > 0) {
                    coords_model.move(coords_list.currentIndex, coords_list.currentIndex - 1, 1);
                }
            }
        }

        Button {
            id: move_down_coord_btn
            text: "Down"

            width: 50
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: move_up_coord_btn.bottom
            anchors.topMargin: 10

            onClicked: {
                console.log(coords_model.count)
                if(coords_list.currentIndex < coords_model.count - 1) {
                    coords_model.move(coords_list.currentIndex, coords_list.currentIndex + 1, 1);
                }
            }
        }

        ListModel {
            id: coords_model
        }

        ScrollView {
            id: coords_list_rect

            background: Rectangle {
                border.width: 2
                radius: 5
                color: "White"
            }

            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: delete_coord_btn.left
            anchors.rightMargin: 10
            anchors.top: input_x_label.bottom
            anchors.topMargin: 10
            anchors.bottom: run_btn.top
            anchors.bottomMargin: 10

            ListView {
                id: coords_list

                focus: true
                highlight: Rectangle {color:"lightsteelblue"}
                clip: true
                anchors.fill: parent
                anchors.margins: 5

                model: coords_model
                delegate: Text {
                    text: "(" + px + ", " + py + ", " + pz + ")"
                    font.pointSize: 12
                    MouseArea {
                        anchors.fill: parent
                        onClicked: coords_list.currentIndex = index
                    }
                }
            }
        }

        Button {
            id: run_btn
            text: "Run"

            width: 50
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.left: parent.left
            anchors.leftMargin: 95
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            onClicked: {
                var coordinates = [];
                for(var i = 0; i < coords_model.count; i++) {
                    coordinates.push(coords_model.get(i).px, coords_model.get(i).py, coords_model.get(i).pz);
                }
                window.close();
                callback(coordinates);
            }
        }

        Button {
            id: cancel_btn
            text: "Cancel"

            width: 50
            height: 30
//            background: Rectangle {
//                color: "White"
//                border.width: 2
//                radius: 5
//            }

            anchors.left: run_btn.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            onClicked: {
                window.close();
            }
        }
    }
}
