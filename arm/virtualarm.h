#ifndef VIRTUALARM_H
#define VIRTUALARM_H

#include <queue>
#include <functional>

#include <core/containers/gmdvector.h>
#include <core/types/gmangle.h>
#include <core/utils/gmrandom.h>

#include <scene/gmscene.h>
#include <scene/sceneobjects/gmpathtrack.h>

#include "../loader/modelloader.h"
#include "armcontroller.h"

#include "alphabet.h"

class Gripper;
class Joint;

struct ArmState {
    GMlib::Angle baseAngle;
    GMlib::Angle shoulderAngle;
    GMlib::Angle elbowAngle;
    GMlib::Angle wristAngle;
    GMlib::Vector<float, 3> basePos;
    GMlib::Vector<float, 3> shoulderPos;
    GMlib::Vector<float, 3> elbowPos;
    GMlib::Vector<float, 3> wristPos;
    GMlib::Vector<float, 3> betweenFingersPos;
    float reward;
};

struct ArmAction {
    GMlib::Angle baseAngle;
    GMlib::Angle shoulderAngle;
    GMlib::Angle elbowAngle;
    GMlib::Angle wristAngle;
};

class VirtualArm : public GMlib::SceneObject
{
    GM_SCENEOBJECT(VirtualArm)
public:

    VirtualArm();
    VirtualArm(ArmController * in_ac);
    VirtualArm(const VirtualArm& copy);
    ~VirtualArm() override;

    void setup(std::shared_ptr<GMlib::Scene> scene);
    void cleanUp();

    void executeActions(std::queue<ArmAction> actions);
    void executeCoords(std::vector<GMlib::Vector<float, 3>> coords);
    void executeParametric(float tmin, float tmax, float dt,
                           std::function<float(float)> xt,
                           std::function<float(float)> yt,
                           std::function<float(float)> zt);

    void setBaseAngle(GMlib::Angle angle);
    void setShoulderAngle(GMlib::Angle angle);
    void setElbowAngle(GMlib::Angle angle);
    void setWristAngle(GMlib::Angle angle);
    void setGripWidth(int space);
    void setSpeed(int speed);
    void doAnimate(bool in_animate){m_animate=in_animate;}
    void shouldGripperTrack(bool shouldTrack) { m_gripperTrack->setVisible(shouldTrack); }

    // AI framework
    ArmState step(ArmAction action, GMlib::Vector<float, 3> desiredPos);
    std::vector<float> step(std::vector<int> action, std::vector<float> desiredPos);
    ArmAction randomAction();
    GMlib::Vector<float, 3> randomPos();
    void randomStep();

    //getters
    GMlib::Vector<float, 3> getEEF();

    //inverse kinematics
    double findGamma(GMlib::Point<float,3> & in_point);
    void moveArmToPoint(GMlib::Point<float,3> in_point);
    ArmAction getActionForPoint(GMlib::Point<float, 3> in_point);
    //direct  kinematics
    void getJointsPoses(std::vector<GMlib::Angle> & in_angles, std::vector<GMlib::Vector<float,3>> & out_poses);
    void getJointsPosesMat(std::vector<GMlib::Angle> & in_angles, std::vector<GMlib::Vector<float,3>> & out_poses);
    GMlib::Vector<float,3> wristHeightCorrection();
    //ik jacobian
    //https://medium.com/unity3danimation/overview-of-jacobian-ik-a33939639ab2
    std::vector<GMlib::Angle> JacobianIK(std::vector<GMlib::Angle> & input_angles, GMlib::Vector<float,3> goal_eef_pos);
    std::vector<GMlib::Angle> GeometryIK(std::vector<GMlib::Angle> & input_angles, GMlib::Vector<float,3> goal_eef_pos);
    std::vector<GMlib::Angle> getDeltaAngles(GMlib::Vector<float,3> & target_pos, GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses);
    GMlib::Matrix<float,3,3> getJacobianTranspose(GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses);

    void switchEefParallel() {m_parallel_eef = !m_parallel_eef;}
    void showJoints(bool shouldShow);
    void showPencil(bool shouldShow);
    void drawSquare();
    void drawInfinity();
    void drawSpiral();
    void drawFlower();
    void drawText(std::string text);

protected:
    void localSimulate(double dt) override;
	
private:
    ArmController* m_arm_controller_ptr;

    std::unordered_map<std::string, Mesh*> m_meshes;
    std::vector<Texture*> m_textures;

    Mesh* m_env;
    Mesh* m_base;
    Mesh* m_shoulder; //0
    Mesh* m_elbow;    //1
    Mesh* m_wrist;    //2
    Gripper* m_gripper;

    GMlib::PathTrack* m_gripperTrack;

    float m_maxRadius = 400.0f; // TODO: Find maxRadius

    bool m_animate = true;
    bool m_isExecuting = false;
    bool m_parallel_eef = false;

    std::queue<ArmAction> m_actionQueue;

    //for testing
    std::vector<std::shared_ptr<Joint>> m_test_joints;

    // AI framework
    float calculateReward(GMlib::Vector<float, 3> desiredPos);
    bool isDone();
    ArmState getState(GMlib::Vector<float, 3> desiredPos);
    bool isPosLegal(GMlib::Vector<float, 3> pos);
    void doAction(ArmAction action);

    GMlib::Random<int> m_randInt;

    double m_chartUpdateRate = 0.2;
    double m_chartUpdateElapsed = 0.0;
};

#endif // VIRTUALARM_H
