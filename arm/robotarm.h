#ifndef ROBOTARM_H
#define ROBOTARM_H

#include <core/containers/gmdvector.h>
#include <core/types/gmangle.h>
#include <scene/gmscene.h>

class Joint;
class Link;

class RobotArm
{
public:
    RobotArm(std::shared_ptr<GMlib::Scene> scene);
    virtual ~RobotArm();

    void setup();
    void remove(std::shared_ptr<GMlib::Scene> scene);

    void setEndPos(GMlib::Vector<float, 3>& pos); //set position of eef
    GMlib::Vector<float, 3> endPos() const;

    //direct kinematics
    GMlib::Vector<float, 3> lastJoint() const;      //get position of last joint gmlib
    GMlib::Vector<float, 3> betweenFingers() const; //get position of center between fingers gmlib

    GMlib::HqMatrix<float,3> lastJointDK() const;        //get position of last joint Direct Kinematics
    GMlib::HqMatrix<float,3> betweenFingersDK() const;   //get position of center between Direct Kinematics

    void getJointsPoses(std::vector<GMlib::Angle> & in_angles, std::vector<GMlib::Vector<float,3>> & out_poses);
    void translateJointsInVector(std::vector<Joint> & copy_joints, size_t from_pos);

    //inverse kinematics
    void moveArmToPoint(GMlib::Point<float,3> in_point);

    // Getters/setters for joints angles
    void setJointAngle(size_t index, int angle);
    int jointAngle(size_t index) const;

    void setBaseAngle(GMlib::Angle angle);
    void setShoulderAngle(GMlib::Angle angle);
    void setElbowAngle(GMlib::Angle angle);
    void setWristAngle(GMlib::Angle angle);
    void setGripWidth(int space);
    void setSpeed(int speed);

    int baseAngle() const;
    int shoulderAngle() const;
    int elbowAngle() const;
    int wristAngle() const;
    float handSpace() const;
    double speed() const;

    void copyObjRotate(GMlib::SceneObject & in_obj, GMlib::Angle angle, GMlib::Angle target_angle, GMlib::Angle current_angle, GMlib::Vector<float, 3> rotate_axel);

    //https://medium.com/unity3danimation/overview-of-jacobian-ik-a33939639ab2
    void JacobianIK(std::vector<GMlib::Angle> & input_angles);
    GMlib::Vector<float,3> getDeltaAngles(GMlib::Vector<float,3> & target_pos, GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses);
//    GMlib::DMatrix<float> getJacobianTranspose();
    GMlib::Matrix<float,3,3> getJacobianTranspose(GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses);

protected:

private:
    std::vector<std::shared_ptr<Joint>> m_joints;
    std::vector<std::shared_ptr<Link>> m_links;
    std::shared_ptr<Joint> m_base_joint;
    std::shared_ptr<Joint> m_end;
    std::shared_ptr<Joint> m_pencilTip;
    std::shared_ptr<Link> m_finger1;
    std::shared_ptr<Link> m_finger2;

    //for testing
    std::vector<std::shared_ptr<Joint>> test_joints;

    std::shared_ptr<GMlib::Scene> m_scene;

    int m_angle;
    float m_handSpace;

    float baseRadius;
    float baseHeight;
    float linkRadius;
    float jointRadius;
    float link1Length;
    float link2Length;
    float link3Length;
    float pencilLength;
    float pencilRadius;

    float gap_base_edge_join1;//offset from base's edge to the center of the joint 1. look from top
    float gap_base_height_join1;//offset from base's top to the center of the joint 1. look from side
};

#endif // ROBOTARM_H
