#ifndef LINK_H
#define LINK_H

#include <parametrics/surfaces/gmpcylinder.h>

class Link : public GMlib::PCylinder<float> {
public:
    using PCylinder::PCylinder;

    ~Link() { /*std::cout << "Link deleted" << std::endl;*/ }

    Link(float radius, float height)
        : PCylinder(radius, radius, height), m_height(height) {}

protected:
    void localSimulate(double dt) override {

    }

private:
    float m_height;
};

#endif // LINK_H
