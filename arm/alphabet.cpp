
#include "alphabet.h"


std::vector<GMlib::Vector<float, 3> > Alphabet::getLetterCoordinates(char search_letter,GMlib::Vector<float,3> start_point, float scale)
{
    std::vector<GMlib::Vector<float, 3> > return_vector;
    //return '-' if letter is not found
    return_vector = (m_letters.find(search_letter)!=m_letters.end())? m_letters.at(search_letter):m_letters.at('_');

    for ( auto & point:return_vector) {
        point[0]*=scale;
        point[1]*=scale;
        point[2]*=scale;
        point +=start_point;
    }

    return return_vector;
}

std::vector<GMlib::Vector<float, 3> > Alphabet::getTextCoordinates(std::string search_text, GMlib::Vector<float,3> start_point, float scale)
{
    std::vector<GMlib::Vector<float, 3> > return_vector;
    std::vector<GMlib::Vector<float, 3> > char_vector;

    char_vector = getLetterCoordinates(' ',start_point,scale);
    return_vector.insert(return_vector.end(),std::begin(char_vector),std::end(char_vector));
    start_point += GMlib::Vector<float, 3> {0.0f,0.0f,m_width*scale};
    for (auto character:search_text) {
        //add letter
        char_vector = getLetterCoordinates(character,start_point,scale);
        return_vector.insert(return_vector.end(),std::begin(char_vector),std::end(char_vector));
        start_point += GMlib::Vector<float, 3> {0.0f,0.0f,m_width*scale};
        //add space
        char_vector = getLetterCoordinates(' ',start_point,scale);
        return_vector.insert(return_vector.end(),std::begin(char_vector),std::end(char_vector));
        start_point += GMlib::Vector<float, 3> {0.0f,0.0f,20.0f*scale};
    }
    return return_vector;
}
