#ifndef REALARM_H
#define REALARM_H

// serial port
#include <QSerialPort>
#include "../serialport/serialportwriter.h"
#include "../serialport/serialconnection.h"

#include <core/types/gmangle.h>

class RealArm
{
public:
    RealArm();
    virtual ~RealArm();

    void setup();

    bool isReal();

    void setBaseAngle(GMlib::Angle angle);
    void setShoulderAngle(GMlib::Angle angle);
    void setElbowAngle(GMlib::Angle angle);
    void setWristAngle(GMlib::Angle angle);
    void setGripWidth(int space);
    void setSpeed(int speed);

    void shouldMove(const int& in_status);

    void connectToArm(const QString & port, const int & baud,const int & data_bits,const int & stop_bits,const int & parity,const int & flow);

private:
    // serial port
    SerialConnection * m_ser_con_pnt;

    int m_speed;
    bool m_execute_arm;

    int   angleToP (const float & in_angle, const int& in_joint_num);
    float pToAngle (const int   & in_p);
    int   spaceToP (const float & in_space );
};

#endif // REALARM_H
