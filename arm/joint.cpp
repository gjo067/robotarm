#include "joint.h"

Joint::Joint(float radius, GMlib::Vector<float, 3> rotAxel)
    : PSphere(radius), m_currAngle(GMlib::Angle(0)), m_targetAngle(GMlib::Angle(0)), m_speed(1.0), m_rotAxel(rotAxel)
{
}

Joint::~Joint()
{
//    std::cout << "Joint deleted" << std::endl;
}

void Joint::setAngle(GMlib::Angle angle)
{
    m_targetAngle = angle;
}

void Joint::setSpeed(double speed)
{
    m_speed = speed / 600.0;
}

void Joint::pseudoSimulate(GMlib::Angle angle)
{
    /* to move at the same moment as function called */
    this->setAngle(angle);
    this->setSpeed(100000.0);
    this->localSimulate(1.0);
}




void Joint::localSimulate(double dt)
{
    // TODO: Prevent rotating the wrong way (ie from min of slider to max)
    // TODO: Use a better way of finding difference of angles.
    // TODO: Implement acceleration?
    double x = m_targetAngle.getRad();
    double y = m_currAngle.getRad();
    double diff = std::atan2(std::sin(x-y), std::cos(x-y)); // https://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
    if(std::abs(diff) > 0.0001) {
        double da = m_speed * dt * ((diff > 0) - (diff < 0));
        if(std::abs(diff) < std::abs(da)) da = diff;
        m_currAngle += da;
        rotate(GMlib::Angle(da), m_rotAxel);
    }
}
