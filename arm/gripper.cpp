#include "gripper.h"

Gripper::Gripper(std::unordered_map<std::string, Mesh*>& meshes, Mesh* parent)
    : m_wrist(parent)
{
    m_space = 5.565f;
    GMlib::Angle startAngle = GMlib::Angle(m_angles[15] * 3.1415926 / 180.0);

    m_rightFinger = meshes.find("GripperRightTrans")->second;
    m_rightFinger->setPos(GMlib::Vector<float, 3>(0, 0, -m_space));
    parent->insert(m_rightFinger);

    m_rightKnuckle = meshes.find("GripperRightRot")->second;
    m_rightKnuckle->translate(GMlib::Vector<float, 3>(41.665f, 0.0f, 28.094f));
    m_rightKnuckle->setRotAxel(GMlib::Vector<float, 3>(0, 1, 0));
    m_rightKnuckle->setAngle(startAngle);
    m_rightFinger->insert(m_rightKnuckle);

    m_leftFinger = meshes.find("GripperLeftTrans")->second;
    m_leftFinger->setPos(GMlib::Vector<float, 3>(0, 0, m_space));
    parent->insert(m_leftFinger);

    m_leftKnuckle = meshes.find("GripperLeftRot")->second;
    m_leftKnuckle->translate(GMlib::Vector<float, 3>(44.965f, 0.0f, -18.625f));
    m_leftKnuckle->setRotAxel(GMlib::Vector<float, 3>(0, 1, 0));
    m_leftKnuckle->setAngle(startAngle);
    m_leftFinger->insert(m_leftKnuckle);

    m_pencil = meshes.find("Pencil")->second;
    m_pencil->translate(GMlib::Vector<float, 3>(72.0f, -50.0f, 4.673f));
    parent->insert(m_pencil);
    m_pencil->setVisible(false);
}

Gripper::~Gripper()
{
    // Delete
}

void Gripper::setGripWidth(int space)
{
    float tSpace = 0.523f * space - 2.28f;
    m_rightFinger->setPos(GMlib::Vector<float, 3>(0, 0, -tSpace));
    m_leftFinger->setPos(GMlib::Vector<float, 3>(0, 0, tSpace));
    m_space = tSpace;

    // TODO: Make rotation follow path and not just shortest path.
    GMlib::Angle angle = GMlib::Angle(m_angles[space] * 3.1415926 / 180);
    m_rightKnuckle->setAngle(angle);
    m_leftKnuckle->setAngle(angle);
}

void Gripper::setSpeed(int speed)
{
    m_rightFinger->setSpeed(speed);
    m_rightKnuckle->setSpeed(speed);
    m_leftFinger->setSpeed(speed);
    m_leftKnuckle->setSpeed(speed);
}

void Gripper::showPencil(bool shouldShow)
{
    m_pencil->setVisible(shouldShow);
}

GMlib::Vector<float, 3> Gripper::getBetweenFingers()
{
    //returns global coordinates of the center between fingers
    auto wrist_local = m_wrist->getPos();

    wrist_local[0]+=m_offset;
    GMlib::Point<float,3> new_point (72.0f,0.0f,0.0f);

    auto wrist_global = m_wrist->getMatrixGlobal()*new_point;

    return wrist_global;
}
