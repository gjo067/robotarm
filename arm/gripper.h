#ifndef HAND_H
#define HAND_H

#include <core/types/gmangle.h>
#include "../loader/modelloader.h"

#include <QDebug>

class Gripper
{
public:
    Gripper(std::unordered_map<std::string, Mesh*>& arm, Mesh* parent);
    ~Gripper();

    void setGripWidth(int space);
    void setSpeed(int speed);
    void showPencil(bool shouldShow);

    GMlib::Vector<float, 3> getBetweenFingers();

    Mesh getLFinger(){return *m_leftFinger;}
    Mesh getLKnuckle(){return *m_leftKnuckle;}
    Mesh getRFinger(){return *m_rightFinger;}
    Mesh getRKnuckle(){return *m_rightKnuckle;}
    float getOffset(){return m_offset;}

private:
    Mesh* m_wrist;
    Mesh* m_rightFinger;
    Mesh* m_rightKnuckle;
    Mesh* m_leftFinger;
    Mesh* m_leftKnuckle;
    Mesh* m_pencil;

    float m_space;
    float m_offset = 72.0f;

    std::vector<double> m_angles = {
        -14.2, -8.52, -5.21, -2.74, -0.657, 1.11, 2.72, 4.09, 5.39, 6.51,
        7.54, 8.52, 9.35, 10.1, 10.8, 11.4, 11.9, 12.3, 12.7, 12.9,
        13, 13.1, 12.9, 12.8, 12.3, 11.8, 11, 10.2, 8.92, 7.37, 5.55
    };
};

#endif // HAND_H
