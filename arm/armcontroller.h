#ifndef ARMCONTROLLER_H
#define ARMCONTROLLER_H

#include <QObject>
#include <QVariant>

class RobotArm;
class RealArm;
class VirtualArm;
class Window;
namespace GMlib {
    class Angle;
}

class ArmController : public QObject
{
    Q_OBJECT
public:
    ArmController();
    virtual ~ArmController() override;

    void setVirtualArm(std::shared_ptr<VirtualArm> virtualArm) {m_virtualArm = virtualArm;}
    void setRealArm(std::shared_ptr<RealArm> realArm) {m_realArm = realArm;}

    std::shared_ptr<RealArm> getRealArm() {return m_realArm;}
    std::shared_ptr<VirtualArm> getVirtualArm() {return m_virtualArm;}

    void setUpSignals(Window& window);
    void cleanUp();

    void updateGUISliders(int in_base_angle, int in_shoulder_angle, int in_elbow_angle, int in_wrist_angle);

signals:
    void baseSliderChange(QVariant in_angle);
    void arm1SliderChange(QVariant in_angle);
    void arm2SliderChange(QVariant in_angle);
    void arm3SliderChange(QVariant in_angle);
    void eefPosChanged(QVariant, QVariant, QVariant);
    void clearPlots();

public slots:
    void setBaseAngle(int angle);
    void setShoulderAngle(int angle);
    void setElbowAngle(int angle);
    void setWristAngle(int angle);
    void setGripWidth(int space);

    void shouldGripperTrack(bool shouldTrack);
    void shouldEefParallel();

    void setSpeed(const int& speed);
    void shouldRealArmsMove(const int& in_status);

    void executeActions(QVariant coords);

    void randomStepArm();
    void drawSquareArm();
    void drawParametricArm();
    void drawSpiralArm();
    void drawText(QString text);

    void showJoints(bool shouldShow);
    void showPencil(bool shouldShow);

    void connectToArm(const QString & port, const int & baud,const int & data_bits,const int & stop_bits,const int & parity,const int & flow);

protected:
    void timerEvent(QTimerEvent *e) override;

private:
    std::shared_ptr<VirtualArm> m_virtualArm;
    std::shared_ptr<RealArm> m_realArm;
};

#endif // ARMCONTROLLER_H
